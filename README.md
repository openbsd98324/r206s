
![](medias/asus-r206s-asia-1-pic.jpg)

Intel Celeron N3050

ASUS VivoBook R206 is designed to be by your side, wherever you go. It weighs just 1.21kg, and easily fits in your bag thanks to a desktop footprint of 193 x 297mm — no larger than an A4 sheet of paper. All your files stay with you, too, thanks to a 500GB HDD.


LSCPU: 
````
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                2
On-line CPU(s) list:   0,1
Thread(s) per core:    1
Core(s) per socket:    2
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 76
Model name:            Intel(R) Celeron(R) CPU  N3050  @ 1.60GHz
Stepping:              3
CPU MHz:               479.980
CPU max MHz:           2160.0000
CPU min MHz:           480.0000
BogoMIPS:              3200.00
Virtualization:        VT-x
L1d cache:             24K
L1i cache:             32K
L2 cache:              1024K
NUMA node0 CPU(s):     0,1
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 movbe popcnt tsc_deadline_timer aes rdrand lahf_lm 3dnowprefetch epb kaiser tpr_shadow vnmi flexpriority ept vpid tsc_adjust smep erms dtherm ida arat
````




````
processor       : 0
vendor_id       : GenuineIntel
cpu family      : 6
model           : 76
model name      : Intel(R) Celeron(R) CPU  N3050  @ 1.60GHz
stepping        : 3
microcode       : 0x362
cpu MHz         : 479.980
cache size      : 1024 KB
physical id     : 0
siblings        : 2
core id         : 0
cpu cores       : 2
apicid          : 0
initial apicid  : 0
fpu             : yes
fpu_exception   : yes
cpuid level     : 11
wp              : yes
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 movbe popcnt tsc_deadline_timer aes rdrand lahf_lm 3dnowprefetch epb kaiser tpr_shadow vnmi flexpriority ept vpid tsc_adjust smep erms dtherm ida arat
bugs            : cpu_meltdown spectre_v1 spectre_v2 mds msbds_only
bogomips        : 3200.00
clflush size    : 64
cache_alignment : 64
address sizes   : 36 bits physical, 48 bits virtual
power management:


processor       : 1
vendor_id       : GenuineIntel
cpu family      : 6
model           : 76
model name      : Intel(R) Celeron(R) CPU  N3050  @ 1.60GHz
stepping        : 3
microcode       : 0x362
cpu MHz         : 479.980
cache size      : 1024 KB
physical id     : 0
siblings        : 2
core id         : 2
cpu cores       : 2
apicid          : 4
initial apicid  : 4
fpu             : yes
fpu_exception   : yes
cpuid level     : 11
wp              : yes
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 movbe popcnt tsc_deadline_timer aes rdrand lahf_lm 3dnowprefetch epb kaiser tpr_shadow vnmi flexpriority ept vpid tsc_adjust smep erms dtherm ida arat
bugs            : cpu_meltdown spectre_v1 spectre_v2 mds msbds_only
bogomips        : 3200.00
clflush size    : 64
cache_alignment : 64
address sizes   : 36 bits physical, 48 bits virtual
power management:


cat /proc/cpuinfo | grep ogo  
bogomips        : 3200.00
bogomips        : 3200.00

mesg | grep lueto
[    3.726420] usb 1-4: Product: Bluetooth Radio 
[   15.886787] Bluetooth: Core ver 2.22
[   15.887005] Bluetooth: HCI device and connection manager initialized
[   15.887217] Bluetooth: HCI socket layer initialized
[   15.887311] Bluetooth: L2CAP socket layer initialized
[   15.887420] Bluetooth: SCO socket layer initialized
[   15.959094] Bluetooth: hci0: rtl: examining hci_ver=06 hci_rev=000a lmp_ver=06 lmp_subver=8821
[   15.959221] Bluetooth: hci0: rtl: loading rtl_bt/rtl8821a_config.bin
[   16.003173] bluetooth hci0: firmware: failed to load rtl_bt/rtl8821a_config.bin (-2)
[   16.003303] bluetooth hci0: Direct firmware load for rtl_bt/rtl8821a_config.bin failed with error -2
[   16.003419] Bluetooth: hci0: Failed to load rtl_bt/rtl8821a_config.bin
[   16.003514] Bluetooth: hci0: rtl: loading rtl_bt/rtl8821a_fw.bin
[   16.003627] bluetooth hci0: firmware: failed to load rtl_bt/rtl8821a_fw.bin (-2)
[   16.003736] bluetooth hci0: Direct firmware load for rtl_bt/rtl8821a_fw.bin failed with error -2
[   16.005654] Bluetooth: hci0: Failed to load rtl_bt/rtl8821a_fw.bin





th hci0: firmware: failed to load rtl_bt/rtl8821a_fw.bin (-2)
[   16.003736] bluetooth hci0: Direct firmware load for rtl_bt/rtl8821a_fw.bin failed with error -2
[   16.005654] Bluetooth: hci0: Failed to load rtl_bt/rtl8821a_fw.bin
[   16.173533] rtl8821ae: Using firmware rtlwifi/rtl8821aefw.bin
[   16.173638] rtl8821ae: Using firmware rtlwifi/rtl8821aefw_wowlan.bin
[   16.173779] rtl8821ae 0000:01:00.0: firmware: failed to load rtlwifi/rtl8821aefw.bin (-2)
[   16.173886] rtl8821ae 0000:01:00.0: Direct firmware load for rtlwifi/rtl8821aefw.bin failed with error -2
[   16.174189] rtl8821ae 0000:01:00.0: firmware: failed to load rtlwifi/rtl8821aefw_wowlan.bin (-2)
[   16.174297] rtl8821ae 0000:01:00.0: Direct firmware load for rtlwifi/rtl8821aefw_wowlan.bin failed with error -2
[   16.309488] ieee80211 phy0: Selected rate control algor


fldelive
    description: Notebook
    product: E202SA (ASUS-NotebookSKU)
    vendor: ASUSTeK COMPUTER INC.
    version: 1.0
    serial: G2NLCX03L90908C
    width: 64 bits
    capabilities: smbios-3.0 dmi-3.0 smp vsyscall32
    configuration: boot=normal chassis=notebook family=E sku=ASUS-NotebookSKU uuid=8E930C4C-62CD-4406-A0C3-4E29ED76BF7B
  *-core
       description: Motherboard
       product: E202SA
       vendor: ASUSTeK COMPUTER INC.
       physical id: 0
       version: 1.0
       serial: BSN12345678901234567
       slot: MIDDLE
     *-firmware
          description: BIOS
          vendor: American Megatrends Inc.
          physical id: 0
          version: E202SA.302
          date: 10/17/2016
          size: 64KiB
          capacity: 6080KiB
          capabilities: pci upgrade shadowing cdboot bootselect socketedrom edd int13floppy1200 int13floppy720 int13floppy2880 int5printscreen int9keyboard int14serial int17printer acpi usb smartbattery biosbootspecification uefi
     *-memory
          description: System Memory
          physical id: b
          slot: System board or motherboard
          size: 2GiB
        *-bank:0
             description: DIMM DDR3 1600 MHz (0.6 ns)
             product: 4KTF25664HZ-1G6E2
             vendor: Micron Technolog
             physical id: 0
             serial: 00000000
             slot: A1_DIMM0
             size: 2GiB
             width: 64 bits
             clock: 1600MHz (0.6ns)
        *-bank:1
             description: DIMM [empty]
             product: Array1_PartNumber1
             vendor: A1_Manufacturer1
             physical id: 1
             serial: A1_SerNum1
             slot: A1_DIMM1
     *-cache:0
          description: L1 cache
          physical id: 12
          slot: CPU Internal L1
          size: 112KiB
          capacity: 112KiB
          capabilities: internal write-back
          configuration: level=1
     *-cache:1
          description: L2 cache
          physical id: 13
          slot: CPU Internal L2
          size: 2MiB
          capacity: 2MiB
          capabilities: internal write-back unified
          configuration: level=2
     *-cpu
          description: CPU
          product: Intel(R) Celeron(R) CPU  N3050  @ 1.60GHz
          vendor: Intel Corp.
          physical id: 14
          bus info: cpu@0
          version: Intel(R) Celeron(R) CPU N3050 @ 1.60GHz
          slot: SOCKET 0
          size: 479MHz
          capacity: 2400MHz
          width: 64 bits
          clock: 80MHz
          capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 movbe popcnt tsc_deadline_timer aes rdrand lahf_lm 3dnowprefetch epb kaiser tpr_shadow vnmi flexpriority ept vpid tsc_adjust smep erms dtherm ida arat cpufreq
          configuration: cores=2 enabledcores=2 threads=2
     *-pci
          description: Host bridge
          product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series SoC Transaction Register
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 21
          width: 32 bits
          clock: 33MHz
          configuration: driver=iosf_mbi_pci
          resources: irq:0
        *-display
             description: VGA compatible controller
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Integrated Graphics Controller
             vendor: Intel Corporation
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 21
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi vga_controller bus_master cap_list rom
             configuration: driver=i915 latency=0
             resources: irq:311 memory:80000000-80ffffff memory:90000000-9fffffff ioport:f000(size=64) memory:c0000-dffff
        *-generic:0
             description: Signal processing controller
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series Power Management Controller
             vendor: Intel Corporation
             physical id: b
             bus info: pci@0000:00:0b.0
             version: 21
             width: 64 bits
             clock: 33MHz
             capabilities: msi pm bus_master cap_list
             configuration: driver=proc_thermal latency=0
             resources: irq:309 memory:81321000-81321fff
        *-storage
             description: SATA controller
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series SATA Controller
             vendor: Intel Corporation
             physical id: 13
             bus info: pci@0000:00:13.0
             version: 21
             width: 32 bits
             clock: 66MHz
             capabilities: storage msi pm ahci_1.0 bus_master cap_list
             configuration: driver=ahci latency=0
             resources: irq:307 ioport:f060(size=32) memory:8131e000-8131e7ff
        *-usb
             description: USB controller
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series USB xHCI Controller
             vendor: Intel Corporation
             physical id: 14
             bus info: pci@0000:00:14.0
             version: 21
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi xhci bus_master cap_list
             configuration: driver=xhci_hcd latency=0
             resources: irq:308 memory:81300000-8130ffff
        *-generic:1 UNCLAIMED
             description: Encryption controller
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series Trusted Execution Engine
             vendor: Intel Corporation
             physical id: 1a
             bus info: pci@0000:00:1a.0
             version: 21
             width: 32 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: latency=0
             resources: memory:81100000-811fffff memory:81000000-810fffff
        *-multimedia
             description: Audio device
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series High Definition Audio Controller
             vendor: Intel Corporation
             physical id: 1b
             bus info: pci@0000:00:1b.0
             version: 21
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: driver=snd_hda_intel latency=0
             resources: irq:312 memory:81310000-81313fff
        *-pci
             description: PCI bridge
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series PCI Express Port #1
             vendor: Intel Corporation
             physical id: 1c
             bus info: pci@0000:00:1c.0
             version: 21
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:306 ioport:e000(size=4096) memory:81200000-812fffff ioport:81400000(size=2097152)
           *-network
                description: Wireless interface
                product: RTL8821AE 802.11ac PCIe Wireless Network Adapter
                vendor: Realtek Semiconductor Co., Ltd.
                physical id: 0
                bus info: pci@0000:01:00.0
                logical name: wlan0
                version: 00
                serial: 1c:b7:2c:5b:93:e7
                width: 64 bits
                clock: 33MHz
                capabilities: pm msi pciexpress bus_master cap_list ethernet physical wireless
                configuration: broadcast=yes driver=rtl8821ae driverversion=4.9.0-11-amd64 firmware=N/A ip=192.168.10.16 latency=0 link=yes multicast=yes wireless=IEEE 802.11
                resources: irq:310 ioport:e000(size=256) memory:81200000-81203fff
        *-isa
             description: ISA bridge
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx Series PCU
             vendor: Intel Corporation
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 21
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master cap_list
             configuration: driver=lpc_ich latency=0
             resources: irq:0
        *-serial
             description: SMBus
             product: Atom/Celeron/Pentium Processor x5-E8000/J3xxx/N3xxx SMBus Controller
             vendor: Intel Corporation
             physical id: 1f.3
             bus info: pci@0000:00:1f.3
             version: 21
             width: 32 bits
             clock: 33MHz
             capabilities: pm cap_list
             configuration: driver=i801_smbus latency=0
             resources: irq:18 memory:81318000-8131801f ioport:f040(size=32)
     *-scsi
          physical id: 1
          logical name: scsi0
          capabilities: emulated
        *-disk
             description: ATA Disk
             product: HGST HTS545050A7
             physical id: 0.0.0
             bus info: scsi@0:0.0.0
             logical name: /dev/sda
             version: A3B0
             serial: RBE50AAH0KZEGP
             size: 465GiB (500GB)
             capabilities: partitioned partitioned:dos
             configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=10201f08
           *-volume:0
                description: NetBSD partition
                physical id: 1
                bus info: scsi@0:0.0.0,1
                logical name: /dev/sda1
                capacity: 1800MiB
                capabilities: primary bootable
           *-volume:1
                description: FreeBSD partition
                physical id: 2
                bus info: scsi@0:0.0.0,2
                logical name: /dev/sda2
                capacity: 2248MiB
                capabilities: primary
           *-volume:2
                description: EXT3 volume
                vendor: Linux
                physical id: 3
                bus info: scsi@0:0.0.0,3
                logical name: /dev/sda3
                version: 1.0
                serial: 1276c863-6f70-4214-869a-b303818f160e
                size: 2248MiB
                capacity: 2248MiB
                capabilities: primary journaled extended_attributes large_files ext3 ext2 initialized
                configuration: created=2022-06-14 11:24:27 filesystem=ext3 lastmountpoint=/target modified=2022-07-12 05:16:19 mounted=2022-06-14 11:24:48 state=clean
           *-volume:3
                description: EXT3 volume
                vendor: Linux
                physical id: 4
                bus info: scsi@0:0.0.0,4
                logical name: /dev/sda4
                logical name: /
                version: 1.0
                serial: ce2054bb-0fe2-4207-8a5a-9fd84b2edf94
                size: 6GiB
                capacity: 6GiB
                capabilities: primary journaled extended_attributes large_files recover ext3 ext2 initialized
                configuration: created=2022-06-14 11:57:19 filesystem=ext3 lastmountpoint=/root modified=2023-01-07 10:38:45 mount.fstype=ext3 mount.options=rw,relatime,data=ordered mounted=2023-01-07 10:38:45 state=mounted



````




